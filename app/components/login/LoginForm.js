import React, { Component } from 'react'
import {
  Container,
  Header,
  Content,
  Form,
  Item,
  Input,
  Label
} from 'native-base'

export default class LoginForm extends Component {
  render () {
    return (
      <Form style={{ marginHorizontal: 15 }}>
        <Item floatingLabel>
          <Label>Surname or Mobile Number</Label>
          <Input />
        </Item>
        <Item floatingLabel last>
          <Label>SmartCard Number</Label>
          <Input />
        </Item>
      </Form>
    )
  }
}
