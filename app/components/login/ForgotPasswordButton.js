import React, { Component } from 'react'
import { Container, Button, Text, StyleProvider } from 'native-base'
import { View } from 'react-native'
import getTheme from '../../../native-base-theme/components'
import material from '../../../native-base-theme/variables/material'

export default class ForgotPasswordButton extends Component {
  render () {
    return (
      <Button small transparent>
        <Text
          style={{
            textAlign: 'right',
            flex: 1,
            flexDirection: 'row',
            justifyContent: 'flex-end',
            alignItems: 'flex-end'
          }}
          uppercase={false}
        >
          Forgot your SmartCard number?
        </Text>
      </Button>
    )
  }
}
