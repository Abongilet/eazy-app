import { Dimensions } from 'react-native'
import EStyleSheet from 'react-native-extended-stylesheet'

const imageWidth = Dimensions.get('window').width / 2

export default EStyleSheet.create({
  container: {
    alignItems: 'center'
  },

  image: {
    width: 125,
    height: 29
  },

  text: {
    width: 360,
    height: 32,
    fontFamily: 'Roboto',
    fontSize: 26,
    color: '#000000',
    textAlign: 'center'
  }
})
