import React, { Component } from 'react'
import { View, Image, Text } from 'react-native'
import styles from './styles'

export default class Logo extends Component {
  render () {
    return (
      <View style={{ alignItems: 'center', paddingTop: 20 }}>
        <Image
          style={{
            width: 145,
            height: 55
          }}
          source={require('../../images/DStv-logo.jpg')}
          resizeMode='contain'
        />
        <Text
          style={{
            width: 360,
            height: 32,
            fontFamily: 'Roboto',
            fontSize: 26,
            color: '#000000',
            textAlign: 'center'
          }}
        >
          Eazy Self Service
        </Text>
      </View>
    )
  }
}
