import React, { Component } from 'react'
import { View, Image } from 'react-native'
import styles from './styles'

export default class Logo extends Component {
  render () {
    return (
      <View
        style={{
          alignItems: 'center',
          paddingTop: 10
        }}
      >
        <Image
          style={{
            width: 250,
            height: 150
          }}
          source={require('../../images/MultiChoice-Africa-22.jpg')}
          resizeMode='contain'
        />
      </View>
    )
  }
}
