import React, { Component } from 'react'
import {
  Container,
  Header,
  Content,
  Button,
  Text,
  StyleProvider
} from 'native-base'

export default class ButtonBlock extends Component {
  render () {
    return (
      <Button
        block
        style={{
          backgroundColor: '#0095da',
          marginHorizontal: 15,
          marginBottom: 10,
          marginTop: 5
        }}
      >
        <Text uppercase={false}>Sign In</Text>
      </Button>
    )
  }
}
