import React, { Component } from 'react'
import { Container, Icon, Button, Text, StyleProvider } from 'native-base'

export default class ButtonIcon extends Component {
  render () {
    return (
      <Button
        iconLeft
        block
        style={{
          backgroundColor: '#00508f',
          marginHorizontal: 15,
          marginTop: 10,
          marginBottom: 10
        }}
      >
        <Icon name='alert' />
        <Text>Fix Errors</Text>
      </Button>
    )
  }
}
