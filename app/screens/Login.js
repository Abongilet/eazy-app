import React, { Component } from 'react';
import { View, StyleSheet, Text } from 'react-native';

import { Container, Content, StyleProvider } from 'native-base';

import Header from '../components/logo/LogoHeader';
import LoginForm from '../components/login/LoginForm';
import ForgotPassword from '../components/login/ForgotPasswordButton';
import Button from '../components/button/Button';
import ButtonIcon from '../components/button/ButtonWithIcon';
import Footer from '../components/logo/LogoFooter';

export default class LoginScreen extends Component {
  handleSignInPress = () => {
    console.log('handle sign in press');
  };

  render() {
    return (
      <View style={{ flex: 1 }}>
        <Container>
          <Content>
            <Header />
            <LoginForm />
            <ForgotPassword />
            <Button />
            <Text
              style={{
                width: 321,
                height: 18,
                fontFamily: 'Roboto',
                fontSize: 12,
                color: '#888888',
                textAlign: 'center',
                flex: 1,
                flexDirection: 'column',
                justifyContent: 'center',
                alignItems: 'center',
                marginLeft: 40,
              }}
            >
              You don't have to register an account before you sign in
            </Text>
            <Text
              style={{
                fontFamily: 'Roboto',
                fontSize: 26,
                color: '#000000',
                textAlign: 'center',
                marginTop: 20,
                flex: 1,
                flexDirection: 'row',
                justifyContent: 'center',
                alignItems: 'center',
              }}
            >
              Eazy Access
            </Text>
            <ButtonIcon />
            <Text
              style={{
                width: 321,
                height: 18,
                fontFamily: 'Roboto',
                fontSize: 12,
                color: '#888888',
                textAlign: 'center',
                marginLeft: 50,
              }}
            >
              Sign in is not required for this function
            </Text>
            <Footer />
          </Content>
        </Container>
      </View>
    );
  }
}
// const styles = StyleSheet.create({
//   belowButton: {
//     width: 321,
//     height: 18,
//     fontFamily: 'Roboto',
//     fonSize: 12,
//     color: '#888888',
//     textAlign: 'center',
//   },
//   eazyAccess: {
//     width: 212,
//     height: 18,
//     fontFamily: 'Roboto',
//     fontSize: 26,
//     color: '#000000',
//     textAlign: 'center',
//   },
// });
