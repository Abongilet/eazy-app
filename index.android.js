import React, { Component } from 'react'
import { AppRegistry, StyleSheet, View } from 'react-native'
import {
  Container,
  Header,
  Content,
  Form,
  Item,
  Input,
  Label,
  Button,
  Text,
  StyleProvider
} from 'native-base'

import LoginScreen from './app/screens/Login'

export default class TestSignIn extends Component {
  // login screen
  render () {
    return <LoginScreen />
  }
}

AppRegistry.registerComponent('TestSignIn', () => TestSignIn)
